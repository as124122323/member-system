
# -*- coding: utf-8 -*-
import ldap
import sys

arg_list = sys.argv
username = arg_list[1]
password = arg_list[2]
print("user: ", username, password)

LDAP_HOST = 'ldap://172.25.55.253:389'
BASEDN = 'ou=login, dc=local, dc=com'
GROUP = 'datareceiver'

ldap_conn = ldap.initialize(LDAP_HOST)
ldap_conn.set_option(ldap.OPT_REFERRALS, 0)
ldap_conn.simple_bind_s('cn=admin,dc=local,dc=com', 'admin')

user_baseDN = 'ou=user, ' + BASEDN
print("user_baseDN", user_baseDN)
searchScope = ldap.SCOPE_SUBTREE
retrieveAttributes = None
searchFilter = "uid=%s" % username
ldap_result_id = ldap_conn.search(user_baseDN, searchScope, searchFilter, retrieveAttributes)
print("ldap_result_id", ldap_result_id)
member_DN = None
while 1:
    result_type, result_data = ldap_conn.result(ldap_result_id, 0)
    print("result_data", result_data)
    print("\n")
    if (result_data == []):
        break
    else:
        if result_type == ldap.RES_SEARCH_ENTRY:
            member_DN = result_data[0][0]
            break
print("member_DN", member_DN)
print("\n")


try:
    auth_res = ldap_conn.simple_bind_s(member_DN, password)
    print("auth_success")
    print("auth_res", auth_res)
except:
    print("auth_failed")
print("\n")

group_baseDN = 'ou=group, ' + BASEDN
searchScope = ldap.SCOPE_SUBTREE
retrieveAttributes = None
searchFilter = "cn=%s" % GROUP
ldap_result_id = ldap_conn.search(group_baseDN, searchScope, searchFilter, retrieveAttributes)
member_list = []
while 1:
    result_type, result_data = ldap_conn.result(ldap_result_id, 0)
    print("result_data2: ", result_data)
    print("\n")
    if (result_data == []):
        break
    else:
        if result_type == ldap.RES_SEARCH_ENTRY:
            member_list = member_list + result_data[0][1]['memberUid']
print("member_list", member_list)
print("\n")

