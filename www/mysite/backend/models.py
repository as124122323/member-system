from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

User = settings.AUTH_USER_MODEL


class UserProfile(AbstractUser):
    authentication_type = models.CharField(max_length=50, verbose_name="驗證方式", default="ldap")

    class Meta:
        verbose_name = "UserProfile"
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.username


class Role(models.Model):
    name = models.CharField(max_length=50, verbose_name="角色名稱")


class Action(models.Model):
    name = models.CharField(max_length=50, verbose_name="動作名稱")
