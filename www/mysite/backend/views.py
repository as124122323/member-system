from django.http import JsonResponse
from django.contrib import auth
from django.forms.models import model_to_dict
import json
import logging
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets

logger = logging.getLogger(__name__)
User = auth.get_user_model()


class UserLoginView(APIView):
    """
        Description: 
            用戶登入
    """
    # Authentications是定義要以什麼機制判斷是否登入
    # Permissions才是真的決定了是否拒絕訪問請求
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = ()
 
    def post(self, request, format=None):
        # request.data 可以處理json, from_data, x-www-form-urlencoded的POST參數
        # json為Dict格式；from_data, x-www-form-urlencoded為QueryDict格式
        post_data = request.data
        logger.info("post_data type: %s" % type(post_data))
        if type(request.data) != dict:
            post_data = post_data.dict()
            
        user = auth.authenticate(**post_data)  # 先驗證
        if user and user.is_active:  # 驗證成功且處於激活狀態則登入
            auth.login(request, user)
            data = model_to_dict(request.user)
            del data["password"]
            return JsonResponse({"status": "success", "data": data, "message": None})
        return JsonResponse({"status": "failed", "data": None, "message": "password is fault, or %s is not register or not active" % post_data["username"]})


class UserCheckLoginView(APIView):
    """
        Description: 
            檢查是否已登入
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = ()
 
    def get(self, request, format=None):
        if request.user.is_authenticated:
            data = model_to_dict(request.user)
            del data["password"]
            return JsonResponse({"status": "success", "data": data, "message": None})
        return JsonResponse({"status": "success", "data": None, "message": "no user login"})


class UserLogoutView(APIView):
    """
        Description: 
            用戶登出
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)  # 在已登入狀態時，才能執行登出
 
    def get(self, request, format=None):
        try:
            auth.logout(request)
        except Exception as e:
            logger.error(e)
            return JsonResponse({"status": "failed", "message": e})
        return JsonResponse({"status": "success"})


class UserRegisterView(APIView):
    """
        Description: 
            用戶註冊
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = ()
 
    def post(self, request, format=None):
        post_data = request.data
        if type(request.data) != dict:
            post_data = post_data.dict()

        user_obj_list = User.objects.filter(username=post_data["username"])
        if len(user_obj_list) > 0:
            msg = "this username (%s) has been registered" % post_data["username"]
            logger.info(msg)
            return JsonResponse({"status": "failed", "message": msg})
        post_data["authentication_type"] = "local"
        User.objects.create_user(**post_data)
        return JsonResponse({"status": "success"})


from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
import datetime
import jwt

class UserResetPasswordView(APIView):
    """
        Description: 
            用戶重設密碼
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = ()
    
    def get(self, request, format=None):
        """
            Description: 判斷token是否可以使用
        """
        try:
            token = request.query_params.dict().get("token", None)
            data = self._decode_reset_password_token(token)
        except jwt.ExpiredSignatureError:
            return JsonResponse({"status": "failed", "message": "驗證超時，請重新操作!"})
        except Exception as e:
            return JsonResponse({"status": "failed", "message": e})

        return JsonResponse({"status": "success", "data": data})

    def post(self, request, format=None):
        """
            Description: 產生token, 並寄出驗證信到用戶信箱
        """
        post_data = request.data
        if type(request.data) != dict:
            post_data = post_data.dict()

        username = post_data.get("username", None)
        email = post_data.get("email", None)

        if not (username and email):
            return JsonResponse({"status": "failed", "message": "username or email need to give"})

        try:
            token = self._generate_reset_password_token(username)
            self._send_email(username, email, token)
            return JsonResponse({"status": "success"})
        except Exception as e:
            return JsonResponse({"status": "failed", "message": e})
    
    def patch(self, request, format=None):
        """
            Description: 解析token, 並更改用戶密碼
        """
        post_data = request.data
        if type(request.data) != dict:
            post_data = post_data.dict()
        
        token = post_data.get("token", None)
        new_password = post_data.get("new_password", None)

        if not token:
            return JsonResponse({"status": "failed", "message": "token need to give"})

        try:
            user_data = self._decode_reset_password_token(token)
        except jwt.ExpiredSignatureError:
            return JsonResponse({"status": "failed", "message": "驗證超時，請重新操作!"})
        except Exception as e:
            return JsonResponse({"status": "failed", "message": e})
        
        if not user_data["username"]:
            return JsonResponse({"status": "failed", "message": "token parse failed"})
        
        user_obj_list = User.objects.filter(username=user_data["username"])
        if len(user_obj_list) != 1:
            msg = "this username (%s) hasn't been registered" % user_data["username"]
            logger.info(msg)
            return JsonResponse({"status": "failed", "message": msg})
        user_obj = user_obj_list[0]
        user_obj.set_password(new_password)
        user_obj.save()
        return JsonResponse({"status": "success"})

    def _send_email(self, username, email, token):
        # 電子郵件內容樣板
        email_template = render_to_string(
            'email/reset_password.html',
            {'username': username, 'url': "http://127.0.0.1:8080/backend/user/reset_password?token=%s" % token}
        )
        email_obj = EmailMessage(
            '重設密碼通知信',  # 電子郵件標題
            email_template,  # 電子郵件內容
            settings.EMAIL_HOST_USER,  # 寄件者
            [email]  # 收件者
        )
        email_obj.content_subtype = 'html'  # 宣告信件樣板是html格式的
        email_obj.fail_silently = False
        email_obj.send()
    
    def _generate_reset_password_token(self, username):
        salt = "test"
        now = datetime.datetime.now()
        expiretime = now + datetime.timedelta(hours = 1)
        payload = {
            "username": username,
            "exp": expiretime.timestamp()
        }
        return jwt.encode(payload, salt, algorithm = 'HS256')
    
    def _decode_reset_password_token(self, token):
        salt = "test"
        return jwt.decode(token, salt, algorithms = ['HS256'])