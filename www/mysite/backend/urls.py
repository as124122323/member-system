from django.urls import path, re_path, include

from backend import views

urlpatterns = [
    path('user/check_login', views.UserCheckLoginView.as_view(), name='user_check_login'),
    path('user/login', views.UserLoginView.as_view(), name='user_login'),
    path('user/logout', views.UserLogoutView.as_view(), name='user_logout'),
    path('user/register', views.UserRegisterView.as_view(), name='user_register'),
    path('user/reset_password', views.UserResetPasswordView.as_view(), name='user_reset_password')
]