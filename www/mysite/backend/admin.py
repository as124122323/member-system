from django.contrib import admin
from .models import UserProfile, Role, Action

# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):
	list_display = ("id", "username", "is_superuser", "is_staff", "is_active")

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Role)
admin.site.register(Action)