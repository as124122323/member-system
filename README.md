# 簡易會員系統API

## 功能簡介：
- 主要框架：Python + Django
- 加入`django-auth-ldap`套件，可透過LDAP方式登入

## 佈署方式（develop）：
1. 取得專案：`git clone https://gitlab.com/as124122323/member-system.git`
2. 進入專案目錄：`cd member-system`
3. 建立Web API Image：`bash docker/build_image.sh`
4. 啟動Docker服務：
	- 啟動指令：`bash docker.develop/deploy_stack.develop.sh`
	- 啟動服務包含：
		- web：Django API
		- db：MySQL DB
		- openldap：簡易LDAP服務系統
		- phpldapadmin：連結LDAP服務系統的可視化界面
	- 示意圖：
		![deploy](https://gitlab.com/as124122323/member-system/-/raw/master/readme_img/%E5%95%9F%E5%8B%95%E6%9C%8D%E5%8B%99.png?inline=false)
	- 若要關閉Docker服務則運行：`bash docker.develop/remove_stack.develop.sh`
5. 第一次啟動需要建立所需的資料表（透過Django migrate）：
	- 指令：
		1. 找出web容器：`docker ps | grep web`
		2. 執行Django migrate：`docker exec -it <web容器ID> bash /var/build_config/python/django_migrate.sh`

## 測試網址（develop）：
- web：
	- domain：你的機器IP 或 127.0.0.1
	- port：8080
- db：
	- domain：你的機器IP 或 127.0.0.1
	- port：8081
	- 帳/密：test / test123
- phpldapadmin：
	- domain：你的機器IP 或 127.0.0.1
	- port：3081
	- 已有用戶資料：username: jamie / password: jamie123
