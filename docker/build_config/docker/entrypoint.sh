#!/usr/bin/bash

# 將Django的靜態檔蒐集起來
source /opt/member_system_env/bin/activate
mkdir -p /var/www/mysite_static
python /var/www/mysite/manage.py collectstatic

# 將apache推到前台，這樣才能保持apache運行狀態
/usr/sbin/apachectl -D FOREGROUND